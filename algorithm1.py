# this script implements algorithm 1 of the paper

from compute_dist_to_set import find_distance
import heapq

def find_min_max_matrix(S, E):

    """
    Calculate the next point from S to be included in E (the approximate convex hull)
    Note: We make a slight deviation from the paper here. S and E are assumed to be mutually exclusive.
    E is the computed approximate convex hull so far
    S is the set of candidate points we have
    Both S and E are lists of d-dimensional vectors
    """

    N = len(S)

    matrix_E = []
    for i in range(N):
        E.append(S[i])
        matrix_E.append((find_distance(S[0], E), i))
        E.pop()
    heapq.heapify(matrix_E)
    min_elem, min_elem_index = heapq.heappop(matrix_E)
    C = [0 for i in range(N)]   # we will use zero based indexing
    C[min_elem_index] += 1
    max_C = 1

    # TODO: check for off by one error
    loop_exec_counter = 0 
    while max_C < N:
        E.append(S[min_elem_index])
        new_dist = find_distance(S[C[min_elem_index]], E)
        E.pop()
        min_elem, min_elem_index = heapq.heappushpop(matrix_E, (max(min_elem, new_dist), min_elem_index))
        C[min_elem_index] += 1
        max_C = max(max_C, C[min_elem_index])
        loop_exec_counter += 1
    print('Loop of find_min_max_matrix executed', loop_exec_counter, 'times.')

    return min_elem, min_elem_index