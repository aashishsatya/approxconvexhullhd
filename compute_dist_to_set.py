# implements the d(z, S) function in the paper,
# i.e. compute the distance from a point z to the set S
# program does this by solving a quadratic optimization problem

from cvxopt import solvers, matrix
import numpy

solvers.options['show_progress'] = False

class Point(object):

    def __init__(self, point_id, point):
        self.point_id = point_id
        self.point = point

dot_dict = {}   # dictionary of dot product

def find_distance(z, S):

    """
    Find distance from the point z (d-dimensional vector) to set S
    (set of d-dimensional vectors)
    Names follow same variable names used in the paper and in cvxopt website
    z an object of class Point now
    S is a list of objects of class Point
    """

    # matrices expressing the quadratic program, see cvxopt manual on quadratic programming

    N = len(S)  # the number of points in the sample

    # print('Computing P...')
    P = numpy.zeros((N, N))
    for i in range(N):
        # print('i =', i, '...')
        for j in range(N):
            if (S[i].point_id, S[j].point_id) in dot_dict:
                P[i][j] = 2 * dot_dict[(S[i].point_id, S[j].point_id)]
            else:
                P[i][j] = numpy.dot(S[i].point, S[j].point)
                dot_dict[(S[i].point_id, S[j].point_id)] = P[i][j]
                dot_dict[(S[j].point_id, S[i].point_id)] = P[i][j]
                P[i][j] *= 2
    P = matrix(P)
    # print('...done.')
    
    q = numpy.zeros(N)
    for i in range(N):
        if (S[i].point_id, z.point_id) in dot_dict:
            q[i] = -2 * dot_dict[(S[i].point_id, z.point_id)]
        else:
            q[i] = numpy.dot(S[i].point, z.point)
            dot_dict[(S[i].point_id, z.point_id)] = q[i]
            dot_dict[(z.point_id, S[i].point_id)] = q[i]
            q[i] *= -2
    q = matrix(q)
    # print('Done computing q...')

    G = numpy.zeros((N, N))
    for i in range(N):
        G[i][i] = -1
    G = matrix(G)
    h = matrix(numpy.zeros(N))

    A = numpy.zeros(N)
    for i in range(N):
        A[i] = 1
    A = matrix(A)
    b = matrix([1], tc = 'd')

    pt_squared = sum([x * x for x in z.point])
    # print('Calling solver...')
    obj_fn = solvers.qp(P, q, G, h, A.T, b)['primal objective']

    return obj_fn + pt_squared