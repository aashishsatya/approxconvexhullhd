# ApproxConvexHullHD

This is an implementation of the paper titled "Computing the approximate convex hull in high dimensions" by Hossein Sartipizadeh and Vincent Tyrone which you can find [here](https://arxiv.org/abs/1603.04422). More details to follow.
