from algorithm1 import find_min_max_matrix
from compute_dist_to_set import find_distance
import random
import time

# zero_distance = 0.001 # consider up to this distance as zero
zero_distance = 0.005 # consider up to this distance as zero

def approx_convex_hull(S, V, reqd_epsilon):
    
    """
    Compute the approximate convex hull of set S that is a (reqd_epsilon)-approximation of the actual convex hull
    or has a maximum V vertices in it
    S: list of d-dimensional vectors
    V: an integer, the maximum number of vertices in the approximate hull
    reqd_epsilon: a real number 
    """

    E = []
    epsilon = reqd_epsilon + 1
    
    while len(S) > 0 and len(E) < V and epsilon > reqd_epsilon:
        print('Finding a candidate point for the hull...')
        candidate_start_time = time.time()
        epsilon, min_elem_index = find_min_max_matrix(S, E)
        candidate_stop_time = time.time()
        new_vertex = S.pop(min_elem_index)
        # print('Added', new_vertex, 'to the list of hulls...')
        print('...candidate found.')
        print('Time taken to find the candidate =', candidate_stop_time - candidate_start_time, 'seconds.')
        E.append(new_vertex)
        # remove points in the interior
        inside_points_indices = []      # indices of points that are in the interior of the convex hull
        # the indices are stored in the decreasing order to keep the indices of the other
        # detected elements the same
        # if lower index elements were removed first, indices of the elements later in the set would change
        print('Removing inside points of the data set...')
        dataset_inside_pts_count = 0
        for i in range(len(S) - 1, -1, -1):
            pt_dist = find_distance(S[i], E)
            # print('Dist =', pt_dist)
            if pt_dist < zero_distance:
                inside_points_indices.append(i)
                dataset_inside_pts_count += 1
        for index in inside_points_indices:
            S.pop(index)
        print('Points removed from data set =', dataset_inside_pts_count)

        if len(E) > 2:
            print('Removing inside points of the approx hull...')
            elem_removed = True
            hull_inside_pts_count = 0
            while elem_removed:
                elem_removed = False
                for i in range(len(E)):
                    if find_distance(E[i], E[:i] + E[i + 1:]) < zero_distance:
                        E = E[:i] + E[i + 1:]
                        elem_removed = True
                        hull_inside_pts_count += 1
                        break
            print('Points removed from hull =', hull_inside_pts_count)

    return E
