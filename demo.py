import random
import main
import time
from compute_dist_to_set import Point

from scipy.spatial import ConvexHull    # for computing the actual convex hull

def generate_unit_hypercube(d):
    
    """
    Generates the vertices of a unit hypercube of d dimensions
    E.g. for d = 2 the program generates
    [[0, 0], [0, 1], [1, 0], [1, 1]]
    """

    # assert d > 0
    if d == 1:
        return [[0], [1]]
    one_dim_less_hypercubes = generate_unit_hypercube(d - 1)
    hypercubes = []
    for one_dim_less_hc in one_dim_less_hypercubes:
        hypercubes.append(one_dim_less_hc + [0])
        hypercubes.append(one_dim_less_hc + [1])
    return hypercubes

def generate_points(d, n):

    """
    Generate n points in d-dimensions from [0, 1]^d, along with
    d-dimensional hypercubes
    """

    # S = generate_unit_hypercube(d)
    S = []
    for _ in range(n):
        S.append([random.random() for j in range(d)])
    random.shuffle(S)
    return S

print('Generating points...')
# S_points = generate_points(3, 100)
S_points = generate_points(1000, 1000)
S = []
for i in range(len(S_points)):
    S.append(Point(i, S_points[i]))
print('...done.')
# reqd_epsilon = 0.005
reqd_epsilon = 0.05
# V = 25
V = 400
start = time.time()
approx_hull = main.approx_convex_hull(S, V, reqd_epsilon)
stop = time.time()
# print('Discovered approx hulls:')
# for hull in approx_hull:
#     print(hull.point)
print('Time taken to compute approx hull =', stop - start, 'seconds.')
print('Number of points in approx hull =', len(approx_hull))

# print('Computing actual convex hull...')
# start = time.time()
# hull = ConvexHull(S)
# stop = time.time()
# print('...done.')
# print('Actual hull:')
# pt_count = 0
# for vertex_index in hull.vertices:
# 	print(S[vertex_index])
# 	pt_count += 1
# print('Time taken to compute actual convex hull =', stop - start, 'seconds')
# print('Number of points in actual hull =', pt_count)
